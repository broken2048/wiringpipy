from distutils.core import setup
from distutils.extension import Extension
from Cython.Build import cythonize

wpp_extension = Extension(
    name="wiringpipy",
    sources=["wiringpipy.pyx"],
    libraries=["wiringPi","pthread"],
    library_dirs=["lib"],
    include_dirs=["lib"],
)
setup(
    name="wiringpipy",
    version="0.0.9",
    author="Ken B",
    ext_modules=cythonize([wpp_extension], compiler_directives={'language_level' : "3"})
)
