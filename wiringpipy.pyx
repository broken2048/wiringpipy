cdef extern from "/usr/include/wiringPi.h":
    int wiringPiSetup()
    void pinMode(int pin, int mode)
    void digitalWrite(int pin, int value)
    void analogWrite(int pin, int value)
cdef extern from "/usr/include/softPwm.h":
    int softPwmCreate(int pin, int initialValue, int pwmRange)
    void softPwmWrite(int pin, int value)
cdef extern from "/usr/include/mcp23008.h":
    extern int mcp23008Setup (const int pinBase, const int i2cAddress)
cdef extern from "/usr/include/mcp23s08.h":
    extern int mcp23s08Setup (const int pinBase, const int spiPort, const int devId)
cdef extern from "/usr/include/mcp23016.h":
    extern int mcp23016Setup (const int pinBase, const int i2cAddress)
cdef extern from "/usr/include/mcp23017.h":
    extern int mcp23017Setup (const int pinBase, const int i2cAddress)
cdef extern from "/usr/include/mcp23s17.h":
    extern int mcp23s17Setup (const int pinBase, const int spiPort, const int devId)
cdef extern from "/usr/include/sr595.h":
    extern int sr595Setup (const int pinBase, const int numPins, const int dataPin, const int clockPin, const int latchPin) 
cdef extern from "/usr/include/pcf8574.h":
    extern int pcf8574Setup (const int pinBase, const int i2cAddress)

def wiringPiSetupPY() -> None:
    wiringPiSetup()

def pinModePY(pin: int,mode: int) -> None:
    pinMode(pin,mode)

def digitalWritePY(pin: int,value: int) -> None:
    digitalWrite(pin,value)

def analogWritePY(pin: int,value: int) -> None:
    analogWrite(pin,value)

def softPwmCreatePY(pin: int, initialValue: int, pwmRange: int) -> None:
    softPwmCreate(pin,initialValue,pwmRange)

def softPwmWritePY(pin: int,value: int) -> None:
    softPwmWrite(pin,value)

def mcp23008SetupPY(pinBase: int, i2cAddress: int) -> None:
    mcp23008Setup(pinBase,i2cAddress)

def mcp23s08SetupPY(pinBase: int, spiPort: int, devId: int) -> None:
    mcp23s08Setup(pinBase,spiPort,devId)

def mcp23016SetupPY(pinBase: int, i2cAddress: int) -> None:
    mcp23016Setup(pinBase,i2cAddress)

def mcp23017SetupPY(pinBase: int, i2cAddress: int) -> None:
    mcp23017Setup(pinBase,i2cAddress)

def mcp23s17SetupPY(pinBase: int, spiPort: int, devId: int) -> None:
    mcp23s17Setup(pinBase,spiPort,devId)

def sr595SetupPY(pinBase: int, numPins: int, dataPin: int, clockPin: int, latchPin: int) -> None:
    sr595Setup(pinBase,numPins,dataPin,clockPin,latchPin)

def pcf8574SetupPY(pinBase: int, i2cAddress: int) -> None:
    pcf8574Setup(pinBase,i2cAddress)

